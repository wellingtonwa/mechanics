'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('users', 'UserController.store')
Route.post('sessions', 'SessionController.store')
Route.post('/files', 'FileController.store')
Route.get('/files/:id', 'FileController.show').middleware('auth')

// Cliente
Route.post('clientes', 'ClienteController.store')
Route.put('clientes/:id', 'ClienteController.edit')
Route.get('clientes/:id', 'ClienteController.show')
Route.get('clientes', 'ClienteController.index')
Route.delete('clientes/:id', 'ClienteController.destroy')
