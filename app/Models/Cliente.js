'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Cliente extends Model {
  contatos () {
    return this.hasMany('App/Models/Contato')
  }
}

module.exports = Cliente
