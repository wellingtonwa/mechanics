'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Cliente = use('App/Models/Cliente')

const defaultPage = {
  perPage: '0',
  page: '1'
}

/**
 * Resourceful controller for interacting with clientes
 */
class ClienteController {
  /**
   * Show a list of all clientes.
   * GET clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const {page, offset} = request.get();
      const clientes = await Cliente.query().paginate(page, offset)
      return clientes;
  }

  /**
   * Create/save a new cliente.
   * POST clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.only(['nome', 'cpf', 'cnpj']);
    const contatos = request.only(['contatos']);
    const cliente = await Cliente.create(data)
    cliente.contatos().saveMany(contatos.contatos);
    return cliente
  }

  /**
   * Display a single cliente.
   * GET clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const cliente = await Cliente.findOrFail(params.id);
    cliente.contatos().fetch();
    return cliente;
  }

  /**
   * Render a form to update an existing cliente.
   * GET clientes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
    const cliente = await Cliente.findOrFail(params.id);
    const data = {...request.only(['nome', 'cpf', 'cnpj']), id: params.id}
    await cliente.merge(data)
    await cliente.save();
    return cliente;
  }

  /**
   * Update cliente details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a cliente with id.
   * DELETE clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const cliente = await Cliente.findOrFail(params.id)
    await cliente.delete()
    return {msg: `Cliente ${cliente.id} - ${cliente.nome}. Apagado!`}
  }
}

module.exports = ClienteController
