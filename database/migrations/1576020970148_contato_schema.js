'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const tiposContato = use('App/Models/TipoContatoEnum')

class ContatoSchema extends Schema {
  up () {
    this.create('contatos', (table) => {
      table.increments()
      table.string('descricao')
      table.enu('tipo_contato', tiposContato)
      table.string('contato')
      table.integer('cliente_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('clientes')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('contatos')
  }
}

module.exports = ContatoSchema
